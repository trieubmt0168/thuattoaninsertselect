package com.dd.trieu.entity;

public class Category {
    int Id;
    String name;
    public Category(int categoryId, String name) {
        super();
        this.Id = categoryId;
        this.name = name;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category [categoryId=" + Id + ", name=" + name + "]";
    }

}