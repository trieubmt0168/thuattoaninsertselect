package com.dd.trieu.entity;

import java.util.ArrayList;

public class Database {
    private ArrayList<Object> productTable;
    private Database instants;

    public Database() {
        this.productTable = new ArrayList<Object>();

    }

    @Override
    public String toString() {
        return "Database{" +
                "productTable=" + productTable +
                ", instants=" + instants +
                '}';
    }

    public int insertTable(String name, Object row) {
        int i = 0;
        if (name.equals("productTable")) {
            this.productTable.add(row);
            i = 1;
        }
        return i;
    }


}