package com.dd.trieu.entity;

public class Product {
    private String name;
    private int quality;
    private int price;
    private int categoryId;
    private String categoryName;

    public Product() {
    }

    public Product(String name, int quality, int price, int categoryId) {
        this.name = name;
        this.quality = quality;
        this.price = price;
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", quality=" + quality +
                ", price=" + price +
                ", categoryId=" + categoryId +
                ",categorynam=" + categoryName+
                '}';
    }
}
