package com.dd.trieu.bai22;
public class Stack
{private Object[] stackArray;
    private int top;
    private int maxSize;

    public Stack(int max) {
        maxSize = max;
        stackArray = new Object[maxSize];
        top = -1;
    }

    public static void main(String[] args) {
        Stack stack = new Stack(3);
        stack.push(1);
        stack.push(2);
        stack.push(3);

        stack.print(stack);
        Object object = stack.get();
        System.out.println("Object : "+object);
        stack.print(stack);

    }

    public void print(Stack stack) {
        for (int i = 0; i <= stack.top; i++) {
            System.out.println(stackArray[i]);
        }
    }

    public void push(Object object) {
        try {
            stackArray[++top] = object;
        }catch (Exception e) {
            // TODO: handle exception
            System.out.println("Mảng đã đầy");
        }

    }

    public Object get() {
        Object obj = new Object();
        try {
            obj = stackArray[top--];
        }catch (Exception e) {
            // TODO: handle exception
            System.out.println("Mảng trống");
        }
        return obj;
    }

}
