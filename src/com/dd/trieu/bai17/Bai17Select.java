package com.dd.trieu.bai17;

import com.dd.trieu.entity.Category;
import com.dd.trieu.entity.Database;
import com.dd.trieu.entity.Product;

import java.util.ArrayList;
import java.util.List;

public class Bai17Select {
    static Database database = new Database();
    private static ArrayList<Product> productList = new ArrayList<>();
    private static ArrayList<Category> categoryList = new ArrayList<>();


    public static void main(String[] args) {
        createProductTest();
        List<Product> products = sortByName(productList);
        System.out.println(products);

    }

    public static void createProductTest() {

        productList.add(new Product("CPU", 10, 750, 1));
        System.out.println("");
        productList.add(new Product("Main", 2, 200, 2));
        productList.add(new Product("RAM", 2, 50, 2));
        productList.add(new Product("HHD", 8, 28, 1));
        productList.add(new Product("SSD", 50, 500, 4));
        productList.add(new Product("KeyBoard", 60, 100, 4));
        productList.add(new Product("Mouse", 40, 50, 4));
        productList.add(new Product("VGA", 3, 700, 3));
        productList.add(new Product("Monitor", 1, 322, 2));
        database.insertTable("productList", productList);
        System.out.printf(" " + productList);

        System.out.println(" ");
        System.out.printf("*******Cateogry********");
        categoryList.add(new Category(1, "Computer"));
        categoryList.add(new Category(2, "Memory"));
        categoryList.add(new Category(3, "Card"));
        categoryList.add(new Category(4, "Accessory"));
        database.insertTable("categoryList", categoryList);
        System.out.println("\n" + categoryList);

    }

    public static List<Product> sortByName(List<Product> listProduct) {
        System.out.println("Xuất");
        List<Product> products = listProduct;
        for (int i = 0; i < listProduct.size(); i++) {
            int k = i;
            for (int j = i + 1; j < listProduct.size(); j++) {
                if (listProduct.get(j).getName().length() > listProduct.get(k).getName().length()) {
                    k = j;
                }
            }
            Product temp = listProduct.get(i);
            products.set(i, products.get(k));
            products.set(k, temp);
        }
        return products;
    }


}
